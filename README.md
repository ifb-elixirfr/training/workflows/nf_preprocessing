
# IFB FAIR bioinfo training - Nextflow workflow

Use during the [IFB FAIR bioinfo training](https://ifb-elixirfr.github.io/IFB-FAIR-bioinfo-training/index.html#home)

## Dataset

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3997237.svg)](https://doi.org/10.5281/zenodo.3997237)

```bash
wget -O FAIR_Bioinfo_data.tar.gz https://zenodo.org/record/3997237/files/FAIR_Bioinfo_data.tar.gz?download=1
```

## Usage

This training workflow is link to this Notebook : [Workflow Initiation](https://gitlab.com/ifb-elixirfr/training/notebooks/workflow_initiation)

### Executor
####  Local

```bash
nextflow run preprocess.nf -profile apptainer
```

#### SLURM 

```bash
srun nextflow run preprocess.nf -profile apptainer,slurm
```

### Manage dependencies

Dependencies are preconfigured in nextflow.config in different profiles

#### Conda

```bash
nextflow run preprocess.nf -profile conda
```

### Apptainer

```bash
nextflow run preprocess.nf -profile apptainer
```

### Docker

⚠️  Not available on a HPC infrastructure.

```bash
nextflow run preprocess.nf -profile docker
```


