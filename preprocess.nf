#!/usr/bin/env nextflow

params.reads_path = "Data/*.fastq.gz"
params.outdir = "results/"

process FASTQC {
    input: 
        tuple val(sample_id), path(reads)

    output:
        path "fastq/*_fastqc.zip", emit: fastqc_ch

    script:
    """
        mkdir fastq 
        fastqc --outdir fastq/ ${reads}
    """
}

process MULTIQC {
    publishDir params.outdir, mode: 'copy'   

    input:
        path '*'

    output:
        path 'multiqc_report.html'

    script:
    """
        multiqc .
    """
}

workflow {
    reads_ch = Channel
                   .fromPath(params.reads_path)
                   .map { file -> tuple(file.getBaseName(2), file) }
    /*reads_ch.view()*/
    FASTQC(reads_ch)
    MULTIQC(FASTQC.out.fastqc_ch.collect())
}
